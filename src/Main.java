import java.util.*;
import java.io.*;
import javax.imageio.*;
import computation.*;

public class Main {

    public static void main(String[] args) {

        Simulation simulation = new Simulation(125,4,-1,2);
        long[] samples = new long[2000];

        for (int i = 0; i < samples.length; ++i) {
            samples[i] = i * 5000;
        }
        double[] E_samples = simulation.run(Site_setter.ALL_UP, samples);
        for (int i = 0; i < samples.length; ++i) System.out.format("E(%s) = %s\n", samples[i], E_samples[i]);

        /*
        Ising_lattice lattice = new Ising_lattice(100, Site_setter.RANDOM_SIGN, 1, 2);
        int n_steps = (int) Math.pow(10, 7);
        int sample_size = n_steps / 5000;
        int lattice_sites = (int) Math.pow(lattice.get_N(), 2);

        try {
            PrintWriter writer = new PrintWriter(new File("results/energy.csv"));
            StringBuilder values = new StringBuilder();

            for (int i = 0; i <= n_steps; i += sample_size) {
                values.append(i);
                values.append(',');
                values.append(lattice.get_E() / lattice_sites);
                values.append('\n');
                lattice.poke(sample_size);
            }
            writer.write(values.toString());
            writer.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        */

        /*
        List<Integer> sample_points = new ArrayList<>();
        int n_images = 100;
        double k = Math.pow(n_steps, 1 / (double) (n_images - 1));
        double sample_point = 1;

        sample_points.add(0);
        sample_points.add(1);
        for (int i = 0; i < n_images - 1; ++i) {
            sample_point *= k;
            sample_points.add((int) sample_point);
        }

        System.out.println(sample_points);
        File image_file;

        try {
            for (int i = 0; i < sample_points.size() - 1; ++i) {
                image_file = new File("results/image_" + (i + 1) + ".png");
                ImageIO.write(lattice.get_bitmap(), "png", image_file);
                lattice.poke(sample_points.get(i + 1) - sample_points.get(i));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        */

        System.out.println("Done.");
    }
}