package computation;

import java.util.*;
import java.util.concurrent.locks.*;
import java.util.concurrent.atomic.DoubleAdder;

/*
Class used for Monte Carlo simulation of an Ising lattice with a spin of 1 (up) or -1 (down) at each of its sites.
There is no external magnetic field in the model.
*/
public class Simulation {

    private Chunk[][] chunks;
    private int m_chunks;

    private final double J; //Interaction parameter J.
    private final double beta; //1 / T * k
    private static final double k = 1.3806503 * Math.pow(10, -23); //Boltzmann's constant

    private long[] samples; //An array of points to sample; time values given in number of steps.
    private DoubleAdder[] E_samples; //An array to store energy values of samples.

    public Simulation(int n, int m_chunks, double J, double T) {

        chunks = new Chunk[m_chunks][];

        for (int i = 0; i < m_chunks; ++i) {
            chunks[i] = new Chunk[m_chunks];
            for (int j = 0; j < m_chunks; ++j) {
                chunks[i][j] = new Chunk(n);
            }
        }
        //setting neighbours
        for (int i = 0; i < m_chunks; ++i) {
            for (int j = 0; j < m_chunks; j++) {
                Chunk[] neighbors = new Chunk[4];

                neighbors[0] = chunks[Lattice.n_mod_m(i - 1, m_chunks)][j];
                neighbors[1] = chunks[i][Lattice.n_mod_m(j + 1, m_chunks)];
                neighbors[2] = chunks[Lattice.n_mod_m(i + 1, m_chunks)][j];
                neighbors[3] = chunks[i][Lattice.n_mod_m(j - 1, m_chunks)];
                chunks[i][j].neighbor_chunks = neighbors;
            }
        }
        this.m_chunks = m_chunks;
        this.J = J;
        beta = 1 / (T * k);
    }

    public double[] run(Site_setter setter, long[] samples) {

        //Set initial state.
        for (int i = 0; i < m_chunks; ++i) {
            for (int j = 0; j < m_chunks; ++j) {
                chunks[i][j].set_sites(setter);
            }
        }

        //Create an array to write energy values to.
        this.samples = samples;
        E_samples = new DoubleAdder[samples.length];

        for (int i = 0; i < E_samples.length; ++i) E_samples[i] = new DoubleAdder();

        //Initialise and start chunk threads.
        Thread[] threads = new Thread[m_chunks * m_chunks];

        for (int i = 0; i < m_chunks; ++i) {
            for (int j = 0; j < m_chunks; ++j) {
                (threads[i * m_chunks + j] = new Thread(chunks[i][j])).start();
            }
        }
        //Wait for threads to finish.
        try {
            for (Thread thread : threads) thread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        //Convert a DoubleAdder array to an array of doubles and return it.
        double[] energies_result = new double[samples.length];

        for (int i = 0; i < E_samples.length; ++i) energies_result[i] = E_samples[i].sum();
        return energies_result;
    }

    private class Chunk extends Lattice implements Runnable {

        private double E; //Interaction energy.

        private Chunk[] neighbor_chunks; /*An array of neighboring chunks in a clockwise order, starting with the
        top one.*/
        private Map<Double, Double> acceptance_ratios; //lookup table of energy differences and acceptance ratios.

        private final ReentrantLock lock; //A lock for thread synchronisation.
        private final Stack<Chunk> locked_chunks; //Used for unlocking locked locks.

        Chunk(int n) {

            super(n);
            neighbor_chunks = new Chunk[4];
            acceptance_ratios = new HashMap<>();
            lock = new ReentrantLock();
            locked_chunks = new Stack<>();
        }

        /*
        Calculates the interaction energy of the chunk. It only accounts for interactions of 4 nearest neighbours for a
        single lattice site and behaves periodically on lattice bounds (as if the lattice was tiled).
        */
        private double calculate_E() {

            double E = 0;

            for (int i = 0; i < n; ++i) {
                for (int j = 0; j < n; ++j) {

                    for (int neighbour : get_neighbors(i, j)) {
                        E += neighbour * get_site(i, j);
                    }
                }
            }
            return -J * E;
        }

        /*
        Attempts to poke (flips a spin) a random site. If the interaction energy after the poke is lower, it accepts the
        poke, otherwise it accepts it with a probability of e^(-beta * (poked lattice's interaction energy - unpoked
        lattice's interaction energy)).
        */
        private void poke() {

            int i = (int) (Math.random() * n);
            int j = (int) (Math.random() * n);

            //Lock necessary neighbor chunks.
            if (i == 0) {
                neighbor_chunks[0].lock.lock();
                locked_chunks.push(neighbor_chunks[0]);
            }
            if (j == n - 1) {
                neighbor_chunks[1].lock.lock();
                locked_chunks.push(neighbor_chunks[1]);
            }
            if (i == n - 1) {
                neighbor_chunks[2].lock.lock();
                locked_chunks.push(neighbor_chunks[2]);
            }
            if (j == 0) {
                neighbor_chunks[3].lock.lock();
                locked_chunks.push(neighbor_chunks[3]);
            }

            double E_difference = calculate_poke_E_difference(i, j);

            if (E_difference <= 0 || Math.random() <= calculate_Acceptance_ratio(E_difference)) {
                //Accept the poke.
                set_site(i, j, -get_site(i, j));
                E += E_difference;
            }

            //Unlock locked neighbor chunks.
            while (!locked_chunks.empty()) locked_chunks.pop().lock.unlock();
        }

        void poke(long n) {
            for (long i = 0; i < n; ++i) poke();
        }

        /*
        Calculates the change in interaction energy resulting from a poke (fliping a spin) at site (given by number of
        row i and number of column j).
        */
        private double calculate_poke_E_difference(int i, int j) {

            double E_difference = 0;

            for (int site : get_neighbors(i, j)) E_difference += site;
            return E_difference * 2 * J * get_site(i, j);
        }

        /*
        Calculates the acceptance ratio. First, it checks in the lookup table of already calculated acceptance ratios,
        if it doesn't find it, it calculates and adds it to the table.
        */
        private double calculate_Acceptance_ratio(double E_difference) {

            if (!acceptance_ratios.containsKey(E_difference)) {
                acceptance_ratios.put(E_difference, Math.exp(-beta * E_difference));
            }
            return acceptance_ratios.get(E_difference);
        }

        @Override
        public void run() {

            E = calculate_E();
            E_samples[0].add(E);
            for (int i = 1; i < samples.length; ++i) {
                poke(samples[i] - samples[i - 1]);
                E_samples[i].add(E);
            }
        }
    }
}