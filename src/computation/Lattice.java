package computation;

//A class representing a state of a N^2 lattice of integers with its top left corner in the coordinate origin.
class Lattice {

    private final int[][] state; //State of the lattice.
    final int n;

    int get_N() {return n;}

    Lattice(int n) {

        state = new int[n][];
        for (int i = 0; i < n; i++) {
            state[i] = new int[n];
        }
        this.n = n;
    }

    //Sets the value of a lattice sites with the given setter function.
    void set_sites(Site_setter setter) {

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; ++j) {
                state[i][j] = setter.set(i, j);
            }
        }
    }

    //Sets the value of a lattice site (given by number of row i and number of column j).
    void set_site(int i, int j, int value) {
        state[i][j] = value;
    }

    //Returns the value of a lattice site (given by number of row i and number of column j).
    int get_site(int i, int j) {
        return state[i][j];
    }

    /*
    Returns a list of values of 4 neighbor** sites of a lattice site (given by number of row i and number of column j)
    in a clockwise order, starting with the top one. It behaves periodically on lattice bounds (as if the lattice was
    tiled).
    */
    int[] get_neighbors(int i, int j) {

        int[] neighbors = new int[4];

        neighbors[0] = state[n_mod_m(i - 1, n)][j];
        neighbors[1] = state[i][n_mod_m(j + 1, n)];
        neighbors[2] = state[n_mod_m(i + 1, n)][j];
        neighbors[3] = state[i][n_mod_m(j - 1, n)];
        return neighbors;
    }

    static int n_mod_m(int n, int m) {

        int mod = n % m;

        if (mod < 0) mod += m;
        return mod;
    }
}