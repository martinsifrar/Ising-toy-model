package computation;

/*
A functional interface containing a method which sets the values of lattice sites (given by number of row i and number
of column j).
*/
public interface Site_setter {

    Site_setter RANDOM_SIGN = (i, j) -> (int) Math.signum(Math.random() - 0.5); //Returns 1 or -1.

    Site_setter ALL_UP = (i, j) -> 1; //Returns 1.

    int set(int i, int j);
}
