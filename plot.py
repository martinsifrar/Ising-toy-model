import matplotlib.pyplot as plot
import pandas

plot.xlabel("Time in steps")
plot.ylabel("Internal energy per site")

values = pandas.read_csv("results\energy.csv", sep = ',', header = None)
#plot.plot([0, values[0].iloc[-1]], [-4, -4], ':k')
plot.plot(values[0], values[1], 'tab:blue')
plot.show()
